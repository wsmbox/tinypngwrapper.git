# laya-cp

#### Description
create laya template project

#### Plan List

- [x] create laya template project (use fgui)
- [x] create laya engine by version:(2.4.0|2.5.0)


#### Instructions

1.  npm i layacp -g

#### Usage

1. In the directory where you want to create a project, Shift + Right click, choose `Open PowerShell window here`
2. input `layacp projectname` or `layacp projectname 2.4.0`

You will get the directory structure as:
```
.
+-- projectname
|   +-- client
|   +-- fguiproject
|   +-- docs
|   +-- readme.txt
```