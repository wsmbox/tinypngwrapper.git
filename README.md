# tinypngwrapper

#### 介绍
使用命令行或者调用node来调用tinypng压图

#### [更新列表](./CHANGELOG.md)

#### 安装教程

1.  npm i tinypngwrapper -g

#### 使用说明

1. 在想要解压的目录中，shift+右键，选择在此处打开PowerShell窗口，输入：`tinyp`，即可将本目录及子目录的图都压了(推荐使用此方式)
2. 在任意目录，打开cmd窗口，输入: `tinyp D:/xxx/`；即可将目录及子目录的图都压了
3. 在任意目录，打开cmd窗口，输入: `tinyp D:/xxx/aa.png`；即可将输入的特定图片压缩
4. 在任意目录，打开cmd窗口，输入: `tinyp -fs D:/xxx/aa.png,D:/bbb/bb.png`；即可将输入的特定图片列表压缩，注意那个 `-fs`

#### 注意

本命令会在本地生成缓存图片，如果想要清除缓存，则调用：`tinyp -ca`