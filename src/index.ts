#! /usr/bin/env node
import { readFileSync } from "fs";
import { join } from "path";
import { compress, clearAllCache } from "./tinypng/Tinypng";


const command = process.argv;

switch (command[2]) {
    case 'help':
    case 'h':
        showHelpStr();
        break;
    case 'version':
    case 'v':
        let verUrl = join(__dirname, '../package.json');
        let verJson = JSON.parse(readFileSync(verUrl).toString())
        console.log('version:', verJson.version);
        break;
    case 'test':
        // testUnzip()
        break;
    case '-ca':
        // 删除所有缓存
        clearAllCache()
        break;
    case '-fs':
        let files: { path: string }[] = [];

        if (command[3]) {
            console.log(command[3]);

            let temps = command[3].split(',');
            files = temps.map(f => {
                return { path: join(f) }
            });
            console.log('files:', files);

            compress(files)
        }
        break;
    default:
        let path = command[2]
        if (!path) {
            path = process.cwd();

            // console.log('path:', path);
            compress(path)
        } else {
            compress(path, command[3]);
        }
        break;
}

function showHelpStr() {
    console.log("==> thank you for using tinypngwrapper <==");
    console.log("if you have question, you can mail me. mail: bfstiger@gmail.com");
    console.log("usage:");

    console.log(`into you want compress dir, input: tinyp`);
}

