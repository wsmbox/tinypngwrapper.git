import { compress, clearAllCache } from "./tinypng/Tinypng";

export { compress, clearAllCache };
