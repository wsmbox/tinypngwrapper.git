import { readdirSync, statSync } from "fs";
import { join, extname } from "path";

export function getAllImgs(path: string, outPaths: string[]) {
    try {
        let stat = statSync(path)
        if (stat.isDirectory()) {
            let files = readdirSync(path);
            files.forEach(f => {
                let ifurl = join(path, f)
                getAllImgs(ifurl, outPaths);
            })
        } else if (stat.isFile()) {
            let ext = extname(path);
            if (ext == '.jpg' || ext == '.png' || ext == '.jpeg') {
                outPaths.push(path);
            }
        }
    } catch (e) {
        console.log(e)
    }
}

